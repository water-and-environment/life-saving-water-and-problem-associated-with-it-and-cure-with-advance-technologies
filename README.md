**Life saving water and problem associated with it and cure with advance technologies**

|Discipline|Physics|
| :- | :- |
|Theme|Water and Environment |
|Module|Life saving water and problem associated with it and cure with advance technologies|


**About the module:**

Life saving water and problem associated with it and cure with advance technologies
 

**Target Audience:**

Class 8 to 12

**Course alignment:**

Physics, Chemistry,Hydrology, Material Science 

**Boards mapped:**

CBSE, ISCE, State Boards

**Language:**

English


|Name of Developer/ principal investigator|Dr. Satanand Mishra|
| :- | :- |
|Institute|CSIR-AMPRI|
|Email. ID|snmishra07@gmail.com|
|Department|CARS & GM|

Contributor List

|Sr. No.|Name |Department|Institute|Email id|
| :- | :- | :- | :- | :- |
|1|Dr. Archana Singh|<p>Centre for Advanced Radiation Shielding and Geopolymeric Materials</p><p></p>|CSIR-AMPRI, Bhopal|archanasingh@ampri.res.in|

